import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { MatFormFieldModule} from '@angular/material/form-field';
import { Validators, FormBuilder } from '@angular/forms';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {
	addfrm:any;
	respuesta:string;
  constructor(private matDialogRef: MatDialogRef<AddItemComponent>,@Inject(MAT_DIALOG_DATA) public data:any,public formBuilder: FormBuilder,private http:Http) {
  	this.addfrm =  this.formBuilder.group({
  		nombre:['', Validators.compose([Validators.maxLength(50), Validators.required])],
  		trabajo:['', Validators.compose([Validators.maxLength(50), Validators.required])],
  	})	
  }

  ngOnInit() {
  	
  }

  public saveItem(){
  	this.http.post('https://reqres.in/api/users',{"name":this.addfrm.value.nombre,"job":this.addfrm.value.trabajo})
  	.pipe(map(res => res.json()))
  	.subscribe(res => {
  		console.log(res)
  		this.addfrm.value.nombre = "";
  		this.addfrm.value.trabajo = "";
  		this.respuesta = JSON.stringify(res);
  		//this.matDialogRef.close()
  	})
  }
  public closeAddItem(){
  	this.matDialogRef.close()
  }


}
