import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { AddItemComponent } from './add-item/add-item.component';
import { EditItemComponent } from './edit-item/edit-item.component';
import { DeleteItemComponent } from './delete-item/delete-item.component';

export interface PeriodicElement {
  id: number;
  first_name: string;
  last_name: string;
  avatar:string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';
  api:string;
  displayedColumns: string[] = ['id', 'first_name', 'last_name','avatar', 'actions'];
  dataSource = [];
  constructor(private http:Http, public dialog: MatDialog){
  	this.api = 'https://reqres.in/api/users';
  	this.http.get(this.api)
  	.pipe(map(res => res.json()))
  	.subscribe(res => {
  		console.log(res)
  		this.dataSource = res.data;
  	})
  }

  public addItem(){
  	this.dialog.open(AddItemComponent, {
      width: '550px',
    });
  }

  public editItem(id){
    this.dialog.open(EditItemComponent, {
      width: '550px',
      data: {id:id}
    });
  }

  public deleteItem(id){
    this.dialog.open(DeleteItemComponent, {
      width: '550px',
      data: {id:id}
    });
  }
}
