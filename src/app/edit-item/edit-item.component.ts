import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { MatFormFieldModule} from '@angular/material/form-field';
import { Validators, FormBuilder } from '@angular/forms';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})
export class EditItemComponent implements OnInit {

  editfrm:any;
	respuesta:string;
	name:string;
	job:string;
  constructor(private matDialogRef: MatDialogRef<EditItemComponent>,@Inject(MAT_DIALOG_DATA) public data:any,public formBuilder: FormBuilder,private http:Http) {
  	this.editfrm =  this.formBuilder.group({
  		nombre:['', Validators.compose([Validators.maxLength(50), Validators.required])],
  		trabajo:['', Validators.compose([Validators.maxLength(50), Validators.required])],
  	})	
  }

  ngOnInit() {
  	this.http.get('https://reqres.in/api/users/'+this.data.id)
  	.pipe(map(res => res.json()))
  	.subscribe(res => {
  		console.log(res)
  		this.name = res.data.first_name;
  		this.job = res.data.job;
  	})
  }

  public editItem(){
  	this.http.put('https://reqres.in/api/users/' + this.data.id ,{"name":this.editfrm.value.nombre,"job":this.editfrm.value.trabajo})
  	.pipe(map(res => res.json()))
  	.subscribe(res => {
  		console.log(res)
  		this.editfrm.value.nombre = "";
  		this.editfrm.value.trabajo = "";
  		this.respuesta = JSON.stringify(res);
  		//this.matDialogRef.close()
  	})
  }
  public closeAddItem(){
  	this.matDialogRef.close()
  }

}
