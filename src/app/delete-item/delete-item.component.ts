import { Component, OnInit,Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-delete-item',
  templateUrl: './delete-item.component.html',
  styleUrls: ['./delete-item.component.css']
})
export class DeleteItemComponent implements OnInit {
	respuesta;
  constructor(private matDialogRef: MatDialogRef<DeleteItemComponent>,@Inject(MAT_DIALOG_DATA) public data:any,private http:Http) {
  	
  }

  ngOnInit() {
  }

  public deleteItem(){
  	this.http.delete('https://reqres.in/api/users/' + this.data.id)
  	.pipe(map(res => res.json()))
  	.subscribe(res => {
  		console.log(res)
  		this.respuesta = JSON.stringify(res);
  		//this.matDialogRef.close()
  	})
  }

  public closeAddItem(){
  	this.matDialogRef.close()
  }
}
